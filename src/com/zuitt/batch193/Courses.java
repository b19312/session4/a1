package com.zuitt.batch193;

public class Courses {

    public String name;

    private String description;

    private int seats;

    private double fee;

    private String startDate;

    private String endDate;

    private User u;

    public Courses(String name, String description, int seats, double fee, String startDate, String endDate){
        this.name = name;
        this.description = description;
        this.seats = seats;
        this.fee = fee;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Courses(){
    this.u = new User("Coleen", "Castro", 23, "Gen tri, Cavite");
    }

    //Getter for user
    public String getUserFirstName(){
        return this.u.getFirstName();
    }

    public String getUserLastName(){
        return this.u.getLastName();
    }

    public int getUserAge(){
        return this.u.getAge();
    }

    public String getUserAddress(){
        return this.u.getAddress();
    }

    //Getter

    public String getName(){
        return this.name;
    }

    public String getDescription(){
        return this.description;
    }

    public int getSeats(){
        return this.seats;
    }

    public double getFee(){
        return this.fee;
    }

    public String getStartDate(){
        return this.startDate;
    }

    public String getEndDate(){
        return this.endDate;
    }

    //Setters
    public void setName(String name){
        this.name = name;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public void setSeats(int seats){
        this.seats = seats;
    }

    public void setFee(double fee){
        this.fee = fee;
    }

    public void setStartDate(String startDate){
        this.startDate = startDate;
    }

    public void setEndDate(String endDate){
        this.endDate = endDate;
    }


}
