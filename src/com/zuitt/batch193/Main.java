package com.zuitt.batch193;

public class Main {

    public static void main(String[] args){

        Courses myFirstCourse = new Courses("Java", "Learn Java", 27, 3000, "June 27, 2022", "July 7, 2022");


        Courses newCourse = new Courses();
        newCourse.setName("Physics 101");
        newCourse.setDescription("Learn Physics");
        newCourse.setSeats(30);
        newCourse.setFee(2000);
        newCourse.setStartDate("June 1, 2022");
        newCourse.setEndDate("June 30, 2022");

        System.out.println("User's first name:");
        System.out.println(newCourse.getUserFirstName());

        System.out.println("User's last name:");
        System.out.println(newCourse.getUserLastName());

        System.out.println("User's age:");
        System.out.println(newCourse.getUserAge());

        System.out.println("User's Address:");
        System.out.println(newCourse.getUserAddress());

        System.out.println("Course's name:");
        System.out.println(newCourse.getName());

        System.out.println("Courses's Description:");
        System.out.println(newCourse.getDescription());

        System.out.println("Course's seats:");
        System.out.println(newCourse.getSeats());

        System.out.println("Course's instructor's first name:");
        System.out.println(newCourse.getUserFirstName());






    }
}
