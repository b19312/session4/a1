package com.zuitt.batch193;

public class User {

    private String firstName;

    private String lastName;

    private int age;

    private String address;

    public User(String firstName, String lastName, int age, String address){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.address = address;
    }

    public User(){

    }

    //Getter
    public String getFirstName(){
        return this.firstName;
    }

    public String getLastName(){
        return this.lastName;
    }

    public int getAge(){
        return this.age;
    }

    public String getAddress(){
        return this.address;
    }
    //Setter
    public String setFirstName(){
        return this.firstName = firstName;
    }

    public String setLastName(){
        return this.lastName = lastName;
    }

    public int setAge(){
        return this.age = age;
    }

    public String setAddress(){
        return this.address = address;
    }
}
